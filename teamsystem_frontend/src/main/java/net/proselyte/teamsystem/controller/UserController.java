package net.proselyte.teamsystem.controller;

import net.proselyte.teamsystem.model.Role;
import net.proselyte.teamsystem.model.User;
import net.proselyte.teamsystem.service.RoleService;
import net.proselyte.teamsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for Admin's pages (list of users, project management, etc.)
 *
 * @author Eugene Suleimanov
 */

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String admin(Model model) {
        return "user/home";
    }

    @RequestMapping(value = "/user/listRoles", method = RequestMethod.GET)
    public String listRoles(Model model) {
        model.addAttribute("role", new Role());
        model.addAttribute("listRoles", this.roleService.getAll());

        return "user/listRoles";
    }
}
