package net.proselyte.teamsystem.controller;

import net.proselyte.teamsystem.model.Role;
import net.proselyte.teamsystem.service.RoleService;
import net.proselyte.teamsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for Admin's pages (list of users, project management, etc.)
 *
 * @author Eugene Suleimanov
 */

@Controller
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String admin(Model model) {
        return "admin/home";
    }

    @RequestMapping(value = "/admin/listRoles", method = RequestMethod.GET)
    public String listRoles(Model model) {
        model.addAttribute("role", new Role());
        model.addAttribute("listRoles", this.roleService.getAll());

        return "admin/role/listRoles";
    }

    @RequestMapping(value = "/admin/addRole", method = RequestMethod.GET)
    public String addRole(Model model) {
        Role role = new Role();
        role.setName("");
        model.addAttribute("role", role);

        return "admin/role/addRole";
    }

    @RequestMapping(value = "/admin/addRole", method = RequestMethod.POST)
    public ModelAndView getRole(@ModelAttribute(name = "role") Role role, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();

        roleService.save(role);

        modelAndView.setViewName("admin/role/listRoles");
        modelAndView.addObject("listRoles", this.roleService.getAll());
        modelAndView.addObject("role", new Role());

        return modelAndView;
    }
}
