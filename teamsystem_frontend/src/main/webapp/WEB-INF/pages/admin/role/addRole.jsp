<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add role</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container margin-top-5">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4"></div>
        <div class="col-lg-4 col-md-4 col-sm-5">
            <form:form method="POST" modelAttribute="role" class="form-signin">
                <h3 class="form-signin-heading">
                    <spring:message code="addRole.head"/>
                </h3>
                <spring:bind path="name">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <spring:message code="addRole.placeholder.namerole" var="NameRole"/>
                        <!--maxlength is set to prevent DataBase exception , NAME CHARACTER VARYING(50) NOT NULL from ROLES table)!-->
                        <form:input type="text" id="ID" onblur='spacesController("ID")' maxlength="50" path="name"
                                    class="form-control" placeholder='${NameRole}' autofocus="true"/>

                        <form:errors path="name" cssStyle="color: red"/>
                    </div>
                </spring:bind>
            </form:form>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-5"></div>
    </div>
</div>
</body>
</html>
