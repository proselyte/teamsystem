package net.proselyte.teamsystem.service;

import net.proselyte.teamsystem.model.User;

import java.util.Collection;
import java.util.UUID;

/**
 * service class for {@link User}
 *
 * @author Eugene Suleimanov
 */

public interface UserService {

    void save(User user);

    void update(User user);

    User getById(UUID id);

    User findByUserName(String username);

    Collection<User> getAll();
}
