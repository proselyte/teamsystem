package net.proselyte.teamsystem.service.impl;

import net.proselyte.teamsystem.dao.RoleDAO;
import net.proselyte.teamsystem.dao.UserDAO;
import net.proselyte.teamsystem.model.Role;
import net.proselyte.teamsystem.model.User;
import net.proselyte.teamsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Implementation of {@link UserService }interface
 *
 * @author Eugene Suleimanov
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDao;

    @Autowired
    private RoleDAO roleDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    @Transactional
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(roleDao.findByName("ROLE_USER"));
        user.setRoles(roles);
        userDao.save(user);
    }

    @Override
    @Transactional
    public void update(User user) {
        Set<Role> roles = user.getRoles();
        user.setRoles(roles);
        userDao.save(user);
    }

    @Override
    @Transactional
    public User getById(UUID id) {
        User user = userDao.getById(id);
        return user;
    }

    @Override
    @Transactional
    public User findByUserName(String username) {
        return userDao.findByUserName(username);
    }

    @Override
    public Collection<User> getAll() {
        return userDao.getAll();
    }
}