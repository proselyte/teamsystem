package net.proselyte.teamsystem.service;

import net.proselyte.teamsystem.model.Role;

import java.util.Collection;
import java.util.UUID;

/**
 * Service interface for class {@link Role}.
 *
 * @author Eugene Suleimanov
 */
public interface RoleService {
    Role getById(UUID id);

    Collection<Role> getAll();

    void save(Role role);

    void remove(Role role);
}
