package net.proselyte.teamsystem.dao;

import net.proselyte.teamsystem.model.BaseEntity;

import java.util.Collection;

/**
 * Generic DAO interface. Used as a base interface for all DAO classes.
 *
 * @author Eugene Suelimanov
 */
interface GenericDAO<T extends BaseEntity, ID> {

    T getById(ID id);

    Collection<T> getAll();

    void save(T entity);

    void remove(T entity);
}