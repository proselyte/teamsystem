package net.proselyte.teamsystem.dao;

import net.proselyte.teamsystem.model.Role;

import java.util.UUID;

/**
 * Extension of {@link GenericDAO} interface for class {@link Role}.
 *
 * @author Eugene Suleimanov
 */
public interface RoleDAO extends GenericDAO<Role, UUID> {
    Role findByName(String name);
}
